from typing import List
from kimiworker import ContentDefinition, ContentLabel, BoundingBox


def create_content_definition(
    text: str,
    *,
    labels: list[ContentLabel] | None = None,
    classification: str = "text",
    confidence: float = 1.0,
    bbox: BoundingBox | None = None,
    children: List[ContentDefinition] | None = None,
) -> ContentDefinition:
    """Helper function to create valid ContentDefinition objects with customizable fields.

    Args:
        text: The text content for the element
        labels: List of content labels, defaults to empty list
        classification: Element classification, defaults to "text"
        confidence: Confidence score, defaults to 1.0
        bbox: Bounding box coordinates, defaults to standard box
        children: Child elements, defaults to None
    """
    return {
        "classification": classification,
        "confidence": confidence,
        "bbox": bbox or {"top": 0.0, "left": 0.0, "right": 1.0, "bottom": 0.1},
        "attributes": {"text": text},
        "labels": labels or [],
        "children": children or [],
    }
