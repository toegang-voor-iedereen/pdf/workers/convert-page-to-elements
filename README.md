# Convert Page to Elements

This python3 worker converts a page to elements.

## Running this worker locally

To run this worker locally on an MacOS device follow the following steps:

- Run `pip install -r requirements.txt`
- Run `AMQP_HOST="127.0.0.1" AMQP_PORT=5672 AMQP_USER="YOUR_AMQP_USER" AMQP_PASS="YOUR_AMQP_PASS" EXCHANGE="page-content" MINIO_HOST="127.0.0.1" MINIO_PORT=9000 MINIO_ACCESS_KEY="YOUR_MINIO_ACCESS_KEY" MINIO_SECRET_KEY="YOUR_MINIO_SECRET_KEY" MINIO_USE_SSL="False" python3 main.py`

Please note it is important to install tesserocr through Conda, as a regular pip install won't work.
