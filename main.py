import os
import sys
from typing import List

from kimiworker import (
    ContentDefinition,
    KimiLogger,
    PublishMethod,
    RemoteFileStorageInterface,
    Worker,
    WorkerJob,
)
from minio import Minio
from shapely import STRtree
from shapely.geometry import Polygon

from content import should_exclude_from_content
from heading import handle_headings
from labels import (
    apply_region_labels,
)
from list import handle_lists
from paragraph import consecutive_lines_to_paragraphs, lines_to_paragraphs
from worker_job import get_interpreted_content, get_regions

numberOfConcurrentJobs = int(os.getenv("CONCURRENT_JOBS", "1"))


def get_page_content(
    elements: List[ContentDefinition], regions: List[ContentDefinition]
) -> List[ContentDefinition]:
    """
    Processes and structures raw page elements into a hierarchical content representation.

    Takes a list of text elements and region definitions and transforms them into a structured
    document by:
    1. Applying region labels based on spatial relationships
    2. Filtering out low confidence and excluded elements
    3. Grouping consecutive list items into nested list structures
    4. Merging related heading elements
    5. Combining consecutive text elements into paragraphs
    6. Converting remaining single lines into paragraphs
    7. Sorting all elements by vertical position

    Args:
        elements (List[ContentDefinition]): Raw text elements extracted from the page
        regions (List[ContentDefinition]): Region definitions with spatial information

    Returns:
        List[ContentDefinition]: Structured and sorted page content with proper hierarchy
        of lists, headings, and paragraphs
    """

    region_polygons = [
        (
            Polygon(
                [
                    [r["bbox"]["left"], r["bbox"]["top"]],
                    [r["bbox"]["right"], r["bbox"]["top"]],
                    [r["bbox"]["right"], r["bbox"]["bottom"]],
                    [r["bbox"]["left"], r["bbox"]["bottom"]],
                    [r["bbox"]["left"], r["bbox"]["top"]],
                ]
            ),
            r,
        )
        for r in regions
    ]
    regions_tree = STRtree([p[0] for p in region_polygons])

    elements.sort(key=lambda item: item["bbox"]["top"])
    for element in elements:
        apply_region_labels(element, regions, regions_tree)

    elements = [el for el in elements if not should_exclude_from_content(el)]

    handle_lists(elements)
    handle_headings(elements)
    consecutive_lines_to_paragraphs(elements)

    elements = [e for e in elements if not e.get("attributes", {}).get("remove")]

    lines_to_paragraphs(elements)

    elements = [e for e in elements if not e.get("attributes", {}).get("remove")]
    elements.sort(key=lambda x: x["bbox"]["top"])
    return elements


def job_handler(
    log: KimiLogger,
    job: WorkerJob,
    job_id: str,
    local_file_path: str,
    remote_file_storage: RemoteFileStorageInterface,
    publish: PublishMethod,
):
    record_id = job.get("recordId")
    page_number = record_id.split("|||")[2]
    elements = get_interpreted_content(job)

    if elements is None:
        publish("error", "interpreted content not found")
        return

    regions = get_regions(job)
    page_content = get_page_content(elements, regions)

    page = [
        {
            "classification": "application/x-nldoc.page",
            "labels": [],
            "confidence": 100,
            "bbox": {"top": 0, "bottom": 1, "left": 0, "right": 1},
            "attributes": {"pageNumber": int(page_number)},
            "children": page_content,
        }
    ]

    publish(
        "contentResult",
        page,
        success=True,
        confidence=100,
    )


if __name__ == "__main__":
    try:  # pragma: no cover
        worker = Worker(job_handler, "worker-page-content", True)
        worker.start(numberOfConcurrentJobs)

    except KeyboardInterrupt:  # pragma: no cover
        print("\n")
        print("Got interruption signal. Exiting...\n")
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
