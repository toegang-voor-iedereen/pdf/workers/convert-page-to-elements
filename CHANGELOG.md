## [1.18.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content/compare/1.18.0...1.18.1) (2025-01-20)


### Bug Fixes

* update base worker ([462c37e](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content/commit/462c37ec9662e4b30dc8195309edcdd770c9c22f))

# [1.18.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content/compare/1.17.0...1.18.0) (2024-12-16)


### Features

* **deps:** kimiworker 4.4.0, removed minio dependency ([eb02c7e](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content/commit/eb02c7e9079a399dc7dfe077166713f32a81711c))

# [1.17.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content/compare/1.16.1...1.17.0) (2024-12-02)


### Bug Fixes

* improve content parsing ([b5989d2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content/commit/b5989d21fc5d9e3a39b736c8447ed09ebcab56d8))
* include element preceding consecutive_indices of has_top_neighbor ([009d6b8](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content/commit/009d6b8fa700cd40c25674d40399154acde67109))


### Features

* add function to filter content ([52f1bf8](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content/commit/52f1bf844fd06493467a9d742598f10825c67da1))
* add function to handle heading creation ([7144b3c](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content/commit/7144b3cdc256f9844d9185099660c721b6aae8c3))
* add functions to handle list parsing ([3a70b06](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content/commit/3a70b060ef91ab13f2667c4d228788210fe20170))
* add functions to handle paragraph parsing ([9b3f226](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content/commit/9b3f226b9327bb4d00c0f3b9b17f59815c6d8c60))

## [1.16.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content/compare/1.16.0...1.16.1) (2024-11-28)


### Bug Fixes

* sort by top position ([1fea04c](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content/commit/1fea04c43c532d820a6889f66ec683f9bb469f3f))

# [1.16.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content/compare/1.15.0...1.16.0) (2024-11-19)


### Features

* force release with kimiworker v3.6.0 ([0f526cb](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content/commit/0f526cbe73a868fba771b4e98b09c8db574ec3d7))

# [1.15.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content/compare/1.14.0...1.15.0) (2024-11-14)


### Features

* heading instead of title ([2a2f63e](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content/commit/2a2f63edc58b6768a6a79352876ddd70e54a9342))

# [1.14.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content/compare/1.13.0...1.14.0) (2024-11-14)


### Features

* filter text with picture label ([0634f5e](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content/commit/0634f5e9ad4b016531a1fe365a3d67792d48cb61))

# [1.13.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content/compare/1.12.0...1.13.0) (2024-11-06)


### Bug Fixes

* use correct content definition tools ([784b898](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content/commit/784b898c7491163ec4747803c5efe9978d99f840))
* use correct merge_labels in labels ([8f1f267](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content/commit/8f1f2676b2a6d0abe788ec2b8b98edf051b08a5b))
* use temp kimiworker version without nldocspec ([1e9ab74](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content/commit/1e9ab7479783052ca3c5b35c81838b1feee8507b))


### Features

* kimiworker 3.3.1 ([7872baa](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content/commit/7872baa32b10cebcdf4b21212bf1ef20a11079d4))
* kimiworker 3.5.0 ([8d19320](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content/commit/8d193206e1da2ee46dcc68ab437e7e44a1bc038b))

# [1.12.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content/compare/1.11.0...1.12.0) (2024-09-11)


### Features

* added volume mount for /tmp ([f61247d](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content/commit/f61247dbfeca689d4ede47b6c78f6284ead31e6c))

# [1.11.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content/compare/1.10.0...1.11.0) (2024-09-04)


### Features

* added securityContext to container and initContainers ([dc46ec1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content/commit/dc46ec116f10cd1616d6e1038f7ca539cdf73e48))

# [1.10.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content/compare/1.9.0...1.10.0) (2024-08-28)


### Features

* use new kimiworker to connect to station 4.0.0 and up ([f21cae5](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content/commit/f21cae5d5b3f619bdc29d2d901fb49c25e5e6909))

# [1.9.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content/compare/1.8.0...1.9.0) (2024-07-04)


### Features

* support initContainers in helm values ([c9677a2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content/commit/c9677a233a01096edc8473518f581b484a8d2533))

# [1.8.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content/compare/1.7.1...1.8.0) (2024-06-05)


### Features

* support initContainers in helm values ([2a3d610](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content/commit/2a3d61003c1cb740a86961d086c3c2db74f395f5))

## [1.7.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content/compare/1.7.0...1.7.1) (2024-06-05)


### Bug Fixes

* properly listen to shutdown signal ([7a43b53](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content/commit/7a43b53ce138211d3585230788f5520331f17632))

# [1.7.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content/compare/1.6.0...1.7.0) (2024-06-04)


### Features

* correct worker name in chart ([afae6d5](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content/commit/afae6d50ab9938e32f628a73b9fc91b4f3322143))
* support nested lists ([5e41a24](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content/commit/5e41a24cc7d9389da17f81948d9fd320e1663b5a))

# [1.6.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-page-to-elements/compare/1.5.0...1.6.0) (2024-05-22)


### Features

* apply heading ([9182dde](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-page-to-elements/commit/9182dde2b771a166bc451d2da055e1dc98308d55))
* better sorting ([806fd87](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-page-to-elements/commit/806fd872a0ab3edb95cd8db350f335303442c53a))
* set page number attribute ([9a61c31](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-page-to-elements/commit/9a61c31ce3754e1880a712700c806ade59800f97))

# [1.5.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-page-to-elements/compare/1.4.0...1.5.0) (2024-05-17)


### Features

* update kimiworker ([d616600](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-page-to-elements/commit/d61660011580c318a21b4ea8bc426d16c61a4b04))

# [1.4.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-page-to-elements/compare/1.3.0...1.4.0) (2024-05-16)


### Features

* publish contentResult ([a4f6b7b](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-page-to-elements/commit/a4f6b7ba80f22c26e151668327bb84985c30727f))

# [1.3.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-page-to-elements/compare/1.2.1...1.3.0) (2024-05-06)


### Features

* filter elements with confidence lower or equal to 50% ([645d153](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-page-to-elements/commit/645d1536586c4cf693bcda839fa11b1d55929db2))
* update kimiworker ([914f047](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-page-to-elements/commit/914f047ee9dce26e3fcf4ec981b4c4d3d109f542))

## [1.2.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-page-to-elements/compare/v1.2.0...1.2.1) (2024-04-24)


### Bug Fixes

* force a release with the new pipeline ([8edfcc5](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-page-to-elements/commit/8edfcc5a08aad3b356d3ca41ce65da20a2e83312))

# [1.2.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-page-to-elements/compare/v1.1.0...v1.2.0) (2024-04-08)


### Features

* add tests for labels ([5a3fdc3](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-page-to-elements/commit/5a3fdc39bf2e9c317fca42080c9099eeb72523bf))
* create paragraphs ([1b37cab](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-page-to-elements/commit/1b37cabf7e9c5042dbf8163c891ceb525ffef152))

# [1.1.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-page-to-elements/compare/v1.0.1...v1.1.0) (2024-04-04)


### Features

* merge regions using STRtree ([6df37cb](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-page-to-elements/commit/6df37cbb9b8ea70aa698791cfccaaf33d7cffc90))
* move confidence ([779afc2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-page-to-elements/commit/779afc238e61c940e8df07309285dfcb92424657))
* prepare for automated releases and tests ([d5a6cf7](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-page-to-elements/commit/d5a6cf7de8f2ac6dc01a7d6db59f710cae0cfa75))
* support new bbox format ([34c80d0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-page-to-elements/commit/34c80d0e8f50943f89789ecbfb8a555491017047))
