from typing import List

from kimiworker import ContentDefinition
import pytest

from list import create_list_element, process_list_elements, handle_lists
from test_utils import create_content_definition


def test_create_list_element():
    """Tests creation of list container element with basic inputs"""
    items: List[ContentDefinition] = [
        {
            "confidence": 90,
            "bbox": {"top": 0.1, "bottom": 0.2, "left": 0.1, "right": 0.9},
        },
        {
            "confidence": 80,
            "bbox": {"top": 0.2, "bottom": 0.3, "left": 0.1, "right": 0.9},
        },
    ]  # type: ignore

    result = create_list_element(items)

    assert result["classification"] == "application/x-nldoc.element.list"
    assert not result["labels"]
    assert result["confidence"] == 85  # mean of 90 and 80
    assert not result["attributes"]
    assert result["children"] == items
    assert result["bbox"] == {
        "top": 0.1,  # min top
        "bottom": 0.3,  # max bottom
        "left": 0.1,  # min left
        "right": 0.9,  # max right
    }


def test_create_list_element_single_item():
    """Tests creation with single item"""
    items: List[ContentDefinition] = [
        {
            "confidence": 90,
            "bbox": {"top": 0.1, "bottom": 0.2, "left": 0.1, "right": 0.9},
        }
    ]  # type: ignore

    result = create_list_element(items)

    assert result["confidence"] == 90
    assert result["bbox"] == items[0]["bbox"]
    assert result["children"] == items


def test_create_list_element_missing_confidence():
    """Tests handling of items without confidence values"""
    items: List[ContentDefinition] = [
        {"bbox": {"top": 0.1, "bottom": 0.2, "left": 0.1, "right": 0.9}},
        {"bbox": {"top": 0.2, "bottom": 0.3, "left": 0.1, "right": 0.9}},
    ]  # type: ignore

    result = create_list_element(items)

    assert result["confidence"] == 0  # Default when confidence missing
    assert result["children"] == items


def test_process_list_elements_single_level():
    """Test processing of single-level list with no nesting"""
    elements = [
        create_content_definition(
            "Item 1",
            bbox={"top": 0.1, "left": 0.1, "right": 0.9, "bottom": 0.2},
            labels=[{"name": "list_item", "confidence": 1.0}],
        ),
        create_content_definition(
            "Item 2",
            bbox={"top": 0.2, "left": 0.1, "right": 0.9, "bottom": 0.3},
            labels=[{"name": "list_item", "confidence": 1.0}],
        ),
    ]

    result = process_list_elements(elements)

    assert len(result) == 2
    assert all(
        e["classification"] == "application/x-nldoc.element.list+item" for e in result
    )
    assert all(not e.get("children") for e in result)


def test_process_list_elements_two_levels():
    """Test processing of two-level nested list"""
    elements = [
        create_content_definition(
            "Item 1",
            bbox={"top": 0.1, "left": 0.1, "right": 0.9, "bottom": 0.2},
            labels=[{"name": "list_item", "confidence": 1.0}],
        ),
        create_content_definition(
            "Subitem 1.1",
            bbox={"top": 0.2, "left": 0.2, "right": 0.9, "bottom": 0.3},
            labels=[{"name": "list_item", "confidence": 1.0}],
        ),
        create_content_definition(
            "Item 2",
            bbox={"top": 0.3, "left": 0.1, "right": 0.9, "bottom": 0.4},
            labels=[{"name": "list_item", "confidence": 1.0}],
        ),
    ]

    result = process_list_elements(elements)

    assert len(result) == 2  # Two top-level items
    assert all(
        e["classification"] == "application/x-nldoc.element.list+item" for e in result
    )

    # Check first item has nested list with proper children
    children = result[0].get("children", [])

    print(result)
    assert children, "First item should have children"
    nested_list = children[0]
    assert nested_list["classification"] == "application/x-nldoc.element.list"

    nested_children = nested_list.get("children", [])
    assert len(nested_children or []) == 1, "Nested list should have one child"
    assert (nested_children or [])[0]["attributes"]["text"] == "Subitem 1.1"


def test_process_list_elements_three_levels():
    """Test processing of three-level nested list"""
    elements = [
        create_content_definition(
            "Item 1",
            bbox={"top": 0.1, "left": 0.1, "right": 0.9, "bottom": 0.2},
            labels=[{"name": "list_item", "confidence": 1.0}],
        ),
        create_content_definition(
            "Subitem 1.1",
            bbox={"top": 0.2, "left": 0.2, "right": 0.9, "bottom": 0.3},
            labels=[{"name": "list_item", "confidence": 1.0}],
        ),
        create_content_definition(
            "Subsubitem 1.1.1",
            bbox={"top": 0.3, "left": 0.3, "right": 0.9, "bottom": 0.4},
            labels=[{"name": "list_item", "confidence": 1.0}],
        ),
    ]

    result = process_list_elements(elements)

    assert len(result) == 1  # One top-level item

    # Check first level nesting
    children = result[0].get("children", [])
    assert children, "Top level item should have children"
    level1_list = children[0]
    assert level1_list["classification"] == "application/x-nldoc.element.list"

    # Check second level nesting
    level1_children = level1_list.get("children", [])
    assert len(level1_children or []) == 1, "First level list should have one child"
    level1_item = (level1_children or [])[0]

    level1_item_children = level1_item.get("children", [])
    assert level1_item_children, "Second level item should have children"

    # Check third level nesting
    level2_list = level1_item_children[0]
    assert level2_list["classification"] == "application/x-nldoc.element.list"
    level2_children = level2_list.get("children", [])
    assert len(level2_children or []) == 1, "Second level list should have one child"
    assert (level2_children or [])[0]["attributes"]["text"] == "Subsubitem 1.1.1"


def test_process_list_elements_mixed_nesting():
    """Test processing of mixed nesting levels"""
    elements = [
        create_content_definition(
            "Item 1",
            bbox={"top": 0.1, "left": 0.1, "right": 0.9, "bottom": 0.2},
            labels=[{"name": "list_item", "confidence": 1.0}],
        ),
        create_content_definition(
            "Subitem 1.1",
            bbox={"top": 0.2, "left": 0.2, "right": 0.9, "bottom": 0.3},
            labels=[{"name": "list_item", "confidence": 1.0}],
        ),
        create_content_definition(
            "Item 2",
            bbox={"top": 0.3, "left": 0.1, "right": 0.9, "bottom": 0.4},
            labels=[{"name": "list_item", "confidence": 1.0}],
        ),
        create_content_definition(
            "Subitem 2.1",
            bbox={"top": 0.4, "left": 0.2, "right": 0.9, "bottom": 0.5},
            labels=[{"name": "list_item", "confidence": 1.0}],
        ),
        create_content_definition(
            "Subitem 2.2",
            bbox={"top": 0.5, "left": 0.2, "right": 0.9, "bottom": 0.6},
            labels=[{"name": "list_item", "confidence": 1.0}],
        ),
    ]

    result = process_list_elements(elements)

    assert len(result) == 2  # Two top-level items

    # Check that both items have children
    for i, item in enumerate(result):
        children = item.get("children", [])
        assert children, f"Item {i+1} should have children"
        sublist = children[0]
        assert sublist["classification"] == "application/x-nldoc.element.list"
        sublist_children = sublist.get("children", [])
        expected_child_count = 1 if i == 0 else 2
        assert (
            len(sublist_children or []) == expected_child_count
        ), f"Sublist {i+1} should have {expected_child_count} children"


def test_process_list_elements_small_indentation():
    """Test processing with small indentation differences"""
    elements = [
        create_content_definition(
            "Item 1",
            bbox={"top": 0.1, "left": 0.1, "right": 0.9, "bottom": 0.2},
            labels=[{"name": "list_item", "confidence": 1.0}],
        ),
        create_content_definition(
            "Item 2",
            bbox={
                "top": 0.2,
                "left": 0.109,
                "right": 0.9,
                "bottom": 0.3,
            },  # Less than 0.01 difference
            labels=[{"name": "list_item", "confidence": 1.0}],
        ),
    ]

    result = process_list_elements(elements)
    assert len(result) == 2  # Should treat as same level
    assert all(not e.get("children") for e in result)  # No nesting


def test_process_list_elements_empty_input():
    """Test handling of empty input"""
    with pytest.raises(IndexError):
        process_list_elements([])


def test_process_list_elements_single_item():
    """Test processing of single item list"""
    elements = [
        create_content_definition(
            "Item 1",
            bbox={"top": 0.1, "left": 0.1, "right": 0.9, "bottom": 0.2},
            labels=[{"name": "list_item", "confidence": 1.0}],
        ),
    ]

    result = process_list_elements(elements)

    assert len(result) == 1
    assert result[0]["classification"] == "application/x-nldoc.element.list+item"
    assert not result[0].get("children")


def test_handle_lists_basic():
    # Create some basic list items
    elements = [
        {
            "labels": [{"name": "list_item"}],
            "bbox": {"left": 10, "top": 10, "right": 100, "bottom": 20},
            "attributes": {},
            "confidence": 0.9,
        },
        {
            "labels": [{"name": "list_item"}],
            "bbox": {"left": 10, "top": 30, "right": 100, "bottom": 40},
            "attributes": {},
            "confidence": 0.9,
        },
    ]

    handle_lists(elements)

    # Check that original elements are marked for removal
    assert elements[0]["attributes"]["remove"] == "true"
    assert elements[1]["attributes"]["remove"] == "true"

    # Check that a new list container was created
    assert len(elements) == 3
    assert elements[2]["classification"] == "application/x-nldoc.element.list"
    assert len(elements[2]["children"]) == 2


def test_process_list_elements_nested():
    elements: list[ContentDefinition] = [
        {
            "classification": "application/x-nldoc.element.list+item",
            "attributes": {"text": "Item 1"},
            "children": [],
            "labels": [],
            "bbox": {"left": 10, "top": 10, "right": 100, "bottom": 0},
            "confidence": 0.9,
        },
        {
            "classification": "application/x-nldoc.element.list+item",
            "attributes": {"text": "Subitem 1.1"},
            "children": [],
            "labels": [],
            "bbox": {"left": 20, "top": 20, "right": 100, "bottom": 0},
            "confidence": 0.9,
        },
        {
            "classification": "application/x-nldoc.element.list+item",
            "attributes": {"text": "Item 2"},
            "children": [],
            "labels": [],
            "bbox": {"left": 10, "top": 30, "right": 100, "bottom": 0},
            "confidence": 0.9,
        },
    ]

    result = process_list_elements(elements)

    # Check structure
    assert len(result) == 2  # Two top-level items
    assert result[0]["classification"] == "application/x-nldoc.element.list+item"
    assert "children" in result[0]
    assert len(result[0]["children"]) == 1  # One subitem


def test_create_list_element_empty():
    result = create_list_element([])

    assert result["classification"] == "application/x-nldoc.element.list"
    assert result["confidence"] == 0
    assert len(result["children"]) == 0
    assert "bbox" in result


def test_create_list_element_with_items():
    items = [
        {
            "confidence": 0.8,
            "bbox": {"top": 10, "bottom": 20, "left": 10, "right": 100},
        },
        {
            "confidence": 0.9,
            "bbox": {"top": 30, "bottom": 40, "left": 10, "right": 100},
        },
    ]

    result = create_list_element(items)

    assert result["classification"] == "application/x-nldoc.element.list"
    assert result["confidence"] == pytest.approx(
        0.85
    )  # Average of 0.8 and 0.9, pytest.approx is used to avoid floating point errors
    assert len(result["children"]) == 2
    assert result["bbox"]["top"] == 10
    assert result["bbox"]["bottom"] == 40


def test_handle_lists_no_list_items():
    elements: list[ContentDefinition] = [
        {
            "classification": "",
            "confidence": 0.9,
            "labels": [{"name": "paragraph", "confidence": 0.9}],
            "bbox": {"left": 10, "top": 10, "right": 100, "bottom": 20},
            "attributes": {},
            "children": [],
        }
    ]

    original_len = len(elements)
    handle_lists(elements)

    # Check that nothing changed
    assert len(elements) == original_len
    assert "remove" not in elements[0]["attributes"]
