from kimiworker import ContentDefinition
from kimiworker.utils.content_definition import merge_labels
from shapely import STRtree
from shapely.geometry import Polygon


def apply_region_labels(
    element: ContentDefinition, regions: list[ContentDefinition], regions_tree: STRtree
):
    """
    Applies region labels to an element and its children based on spatial relationships.

    Takes a content element and assigns it labels from any regions that spatially overlap
    with it by:
    1. Creating a polygon from the element's bounding box
    2. Finding overlapping regions using an R-tree spatial index
    3. Merging the element's existing labels with labels from matching regions
    4. Recursively processing any child elements

    Args:
        element (ContentDefinition): Element to label
        regions (list[ContentDefinition]): List of region definitions with labels
        regions_tree (STRtree): Spatial index of region polygons for efficient lookup

    Side Effects:
        - Modifies element's labels list in-place to include region labels
        - Recursively updates labels of all child elements
    """
    element_bbox = element.get("bbox")
    element_polygon = Polygon(
        [
            [element_bbox.get("left"), element_bbox.get("top")],
            [element_bbox.get("right"), element_bbox.get("top")],
            [element_bbox.get("right"), element_bbox.get("bottom")],
            [element_bbox.get("left"), element_bbox.get("bottom")],
            [element_bbox.get("left"), element_bbox.get("top")],
        ]
    )
    region_indices: list[int] = regions_tree.query(element_polygon).tolist()
    region_labels = [regions[i].get("labels") or [] for i in region_indices]
    labels = element.get("labels") or []
    merged_labels = merge_labels(labels, region_labels)
    element.update(({"labels": merged_labels}))
    for child in element.get("children") or []:
        apply_region_labels(child, regions, regions_tree)
