from statistics import mean
from typing import List

from kimiworker import (
    ContentDefinition,
    BoundingBox,
    get_enclosing_bbox,
)
from kimiworker.utils.content_definition import (
    find_consecutive_element_indices_with_label,
)


def handle_lists(elements: List[ContentDefinition]):
    """
    Groups consecutive list items into hierarchical list structures.

    Processes the provided elements to find sequences of list items and convert
    them into proper list structures by:
    1. Finding consecutive elements labeled as list items
    2. Processing them into nested structures based on indentation
    3. Creating container list elements to hold the items
    4. Marking original list item elements for removal

    Args:
        elements (List[ContentDefinition]): List of document elements to process.
            Modified in-place to contain the new list structures.

    Side Effects:
        - Marks original list item elements for removal by setting attributes['remove']
        - Appends new list container elements to the input list
        - Original list items remain in list but marked for removal
    """
    list_indices = find_consecutive_element_indices_with_label(elements, ["list_item"])
    for indices in list_indices:
        start, end = indices[0], indices[-1] + 1
        consecutive = elements[start:end]
        nested = process_list_elements(consecutive)
        list_elem = create_list_element(nested)

        for element in consecutive:
            element["attributes"]["remove"] = "true"
        elements.append(list_elem)


def process_list_elements(
    consecutive_elements: List[ContentDefinition],
) -> List[ContentDefinition]:
    """
    Recursively processes list elements to create a hierarchical list structure based on indentation.

    Takes a sequence of list items and organizes them into a nested structure by:
    1. Using the left position of elements to determine nesting level
    2. Converting each element into a list item
    3. Recursively processing nested items
    4. Attaching nested lists as children of their parent items

    Args:
        consecutive_elements (List[ContentDefinition]): A sequence of list elements to process.
            Each element must have a 'bbox' attribute with a 'left' coordinate. Elements will
            be modified in-place to have list item classification.

    Returns:
        List[ContentDefinition]: A list of processed elements with proper nesting structure.
            Elements at the same indentation level are siblings, while more indented elements
            become children of the previous less-indented element.

    Side Effects:
        - Modifies each element in-place to have classification 'application/x-nldoc.element.list+item'
        - Creates new list container elements for nested lists
        - Modifies element children arrays to establish hierarchy

    Example:
        >>> elements = [
        ...     ContentDefinition(bbox={"left": 0.1}, text="Item 1"),
        ...     ContentDefinition(bbox={"left": 0.2}, text="Subitem 1.1"),
        ...     ContentDefinition(bbox={"left": 0.1}, text="Item 2")
        ... ]
        >>> result = process_list_elements(elements)
        >>> # Results in:
        >>> # - Item 1
        >>> #   - Subitem 1.1
        >>> # - Item 2
    """
    result: List[ContentDefinition] = []
    temp: List[ContentDefinition] = []
    first_item_left = consecutive_elements[0].get("bbox").get("left")

    for element in consecutive_elements:
        element.update({"classification": "application/x-nldoc.element.list+item"})
        if abs(first_item_left - element.get("bbox").get("left")) > 0.01:
            temp.append(element)
        else:
            if temp:
                nested_result = process_list_elements(temp)
                if result:
                    children = result[-1].setdefault("children", [])
                    if children is not None:
                        children.append(create_list_element(nested_result))
                temp = []
            result.append(element)

    if temp:
        nested_result = process_list_elements(temp)
        if result:
            children = result[-1].setdefault("children", [])
            if children is not None:
                children.append(create_list_element(nested_result))

    return result


def create_list_element(elements: List[ContentDefinition]) -> ContentDefinition:
    """
    Creates a container element to group a sequence of list items.

    Constructs a parent list element that encapsulates multiple list items by:
    1. Using the standard list classification
    2. Calculating average confidence from child elements
    3. Computing a bounding box that encloses all child elements
    4. Setting the provided elements as children

    Args:
        elements (List[ContentDefinition]): The list items to group together.

    Returns:
        ContentDefinition: A new list container
    """
    if not elements:
        confidence = 0
        bbox: BoundingBox = {"top": 0, "bottom": 0, "left": 0, "right": 0}
    else:
        confidence = mean([element.get("confidence", 0) for element in elements])
        bbox = get_enclosing_bbox(elements)

    return {
        "classification": "application/x-nldoc.element.list",
        "labels": [],
        "confidence": confidence,
        "bbox": bbox,
        "attributes": {},
        "children": elements,
    }
