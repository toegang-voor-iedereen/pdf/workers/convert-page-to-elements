from typing import List
from statistics import mean

from paragraph import (
    should_include_previous_element,
    consecutive_lines_to_paragraphs,
    lines_to_paragraphs,
)
from kimiworker import ContentDefinition
from test_utils import create_content_definition


def test_lines_to_paragraphs():
    """Tests basic conversion of text lines to paragraphs"""
    elements = [
        create_content_definition(
            "Line 1",
            classification="application/x-nldoc.element.text+line",
            confidence=90,
            labels=[{"name": "text", "confidence": 1.0}],
        )
    ]

    lines_to_paragraphs(elements)

    assert len(elements) == 2  # Original + new paragraph
    assert elements[0]["attributes"]["remove"] == "true"  # Original marked for removal

    paragraph = elements[1]
    assert paragraph["classification"] == "application/x-nldoc.element.text+paragraph"
    assert paragraph["labels"] == elements[0]["labels"]
    assert paragraph["confidence"] == 90
    assert paragraph["bbox"] == elements[0]["bbox"]
    assert paragraph["attributes"]["text"] == "Line 1"
    assert len(paragraph["children"] or []) == 1


def test_lines_to_paragraphs_multiple():
    """Tests handling multiple lines"""
    elements = [
        create_content_definition(
            "Line 1",
            classification="application/x-nldoc.element.text+line",
            confidence=90,
            labels=[{"name": "text", "confidence": 1.0}],
        ),
        create_content_definition(
            "Line 2",
            classification="application/x-nldoc.element.text+line",
            confidence=85,
            labels=[{"name": "text", "confidence": 1.0}],
        ),
    ]

    lines_to_paragraphs(elements)

    assert len(elements) == 4  # Two originals + two paragraphs
    assert all(e["attributes"]["remove"] == "true" for e in elements[:2])
    assert all(
        e["classification"] == "application/x-nldoc.element.text+paragraph"
        for e in elements[2:]
    )


def test_lines_to_paragraphs_mixed_content():
    """Tests handling mix of lines and other elements"""
    elements = [
        create_content_definition(
            "Line 1",
            classification="application/x-nldoc.element.text+line",
        ),
        create_content_definition(
            "Existing Paragraph",
            classification="application/x-nldoc.element.text+paragraph",
        ),
    ]

    lines_to_paragraphs(elements)

    assert len(elements) == 3  # Original two + one new paragraph
    assert elements[0]["attributes"]["remove"] == "true"
    assert "remove" not in elements[1]["attributes"]  # Non-line element untouched


def test_lines_to_paragraphs_missing_attributes():
    """Tests handling of missing attributes and values"""
    elements = [
        create_content_definition(
            "",
            classification="application/x-nldoc.element.text+line",
            confidence=0,
            labels=[],
        )
    ]

    lines_to_paragraphs(elements)

    paragraph = elements[1]
    assert paragraph["labels"] == []
    assert paragraph["confidence"] == 0
    assert paragraph["attributes"]["text"] == ""


def test_lines_to_paragraphs_empty():
    """Tests handling of empty input"""
    elements: List[ContentDefinition] = []
    lines_to_paragraphs(elements)
    assert len(elements) == 0


def test_should_include_previous_element_excluded_labels():
    """Test that elements with excluded labels are not included"""
    curr_element = create_content_definition("also normal length")

    for excluded_label in [
        "table",
        "picture",
        "page_footer",
        "section_header",
        "title",
        "page_number",
    ]:
        prev_element = create_content_definition(
            "normal length text",
            labels=[{"name": excluded_label, "confidence": 0.9}],
        )
        assert not should_include_previous_element(prev_element, curr_element)


def test_should_include_previous_element_list_item_handling():
    """Test list item handling logic"""
    prev_element = create_content_definition(
        "list item text",
        labels=[{"name": "list_item", "confidence": 0.9}],
    )

    # Should not include list item if current is not a list item
    curr_element = create_content_definition("normal text")
    assert not should_include_previous_element(prev_element, curr_element)

    # Should include list item if current is also a list item
    curr_element = create_content_definition(
        "normal text",
        labels=[{"name": "list_item", "confidence": 0.9}],
    )
    assert should_include_previous_element(prev_element, curr_element)


def test_should_include_previous_element_line_length_ratio():
    """Test the line length ratio check"""
    curr_element = create_content_definition("this is a much longer line of text")

    # Too short previous line
    prev_element = create_content_definition("short")
    assert not should_include_previous_element(prev_element, curr_element)

    # Acceptable length ratio
    prev_element = create_content_definition("this is also a reasonably long line")
    assert should_include_previous_element(prev_element, curr_element)


def test_should_include_previous_element_valid_paragraph_elements():
    """Test normal case where elements should be combined"""
    prev_element = create_content_definition("This is a normal paragraph line.")
    curr_element = create_content_definition("This is another normal line.")
    assert should_include_previous_element(prev_element, curr_element)


def test_should_include_previous_element_multiple_labels():
    """Test handling of multiple labels"""
    prev_element = create_content_definition(
        "normal text",
        labels=[
            {"name": "text", "confidence": 0.9},
            {"name": "table", "confidence": 0.8},
        ],
    )
    curr_element = create_content_definition("normal text")
    assert not should_include_previous_element(prev_element, curr_element)


def test_should_include_previous_element_missing_optional_fields():
    """Test handling of missing fields that are actually used by the function"""
    curr_element = create_content_definition("normal text")

    # Missing labels should be treated as empty list
    prev_element_no_labels: ContentDefinition = {
        "classification": "text",
        "confidence": 1.0,
        "bbox": {"top": 0.0, "left": 0.0, "right": 1.0, "bottom": 0.1},
        "attributes": {"text": "normal text"},
        "labels": None,
        "children": None,
    }
    assert should_include_previous_element(prev_element_no_labels, curr_element)

    # Missing attributes should return False
    prev_element_no_attributes = create_content_definition("normal text")
    prev_element_no_attributes["attributes"] = {}
    assert not should_include_previous_element(prev_element_no_attributes, curr_element)


def test_should_include_previous_element_missing_text_attribute():
    """Test handling of missing text attribute"""
    curr_element = create_content_definition("normal text")
    prev_element_no_text = create_content_definition("normal text")
    del prev_element_no_text["attributes"]["text"]
    assert not should_include_previous_element(prev_element_no_text, curr_element)


def test_should_include_previous_element_basic():
    prev_element: ContentDefinition = create_content_definition(
        "Previous text",
        classification="application/x-nldoc.element.text+line",
        confidence=90,
        labels=[{"name": "text", "confidence": 1.0}],
    )
    curr_element: ContentDefinition = create_content_definition(
        "Current text",
        classification="application/x-nldoc.element.text+line",
        confidence=90,
        labels=[{"name": "text", "confidence": 1.0}],
    )

    assert should_include_previous_element(prev_element, curr_element) == True


def test_should_include_previous_element_list_items():
    # Test when current is not list item but previous is
    prev_element: ContentDefinition = create_content_definition(
        "Previous text",
        classification="application/x-nldoc.element.list-item",
        confidence=90,
        labels=[{"name": "list_item", "confidence": 1.0}],
    )
    curr_element: ContentDefinition = create_content_definition(
        "Current text",
        classification="application/x-nldoc.element.text+line",
        confidence=90,
        labels=[{"name": "text", "confidence": 1.0}],
    )
    assert should_include_previous_element(prev_element, curr_element) == False

    # Test when both are list items
    curr_element["labels"] = [{"name": "list_item", "confidence": 1.0}]
    assert should_include_previous_element(prev_element, curr_element) == True


def test_should_include_previous_element_empty_text():
    # Test with empty previous text
    prev_element: ContentDefinition = create_content_definition(
        "",
        classification="application/x-nldoc.element.text+line",
        confidence=90,
        labels=[{"name": "text", "confidence": 1.0}],
    )
    curr_element: ContentDefinition = create_content_definition(
        "Current text",
        classification="application/x-nldoc.element.text+line",
        confidence=90,
        labels=[{"name": "text", "confidence": 1.0}],
    )
    assert should_include_previous_element(prev_element, curr_element) == False

    # Test with empty current text
    prev_element["attributes"]["text"] = "Previous text"
    curr_element["attributes"]["text"] = ""
    assert should_include_previous_element(prev_element, curr_element) == False

    # Test with no text attribute
    prev_element["attributes"] = {}
    curr_element["attributes"] = {}
    assert should_include_previous_element(prev_element, curr_element) == False


def test_should_include_previous_element_length_ratio():
    # Test with short previous text (ratio < 0.4)
    prev_element: ContentDefinition = create_content_definition(
        "Short",
        classification="application/x-nldoc.element.text+line",
        confidence=90,
        labels=[{"name": "text", "confidence": 1.0}],
    )
    curr_element: ContentDefinition = create_content_definition(
        "This is a much longer piece of text",
        classification="application/x-nldoc.element.text+line",
        confidence=90,
        labels=[{"name": "text", "confidence": 1.0}],
    )

    assert should_include_previous_element(prev_element, curr_element) == False

    # Test with acceptable length ratio
    prev_element["attributes"]["text"] = "This is also a longer piece of text"
    assert should_include_previous_element(prev_element, curr_element) == True


def test_consecutive_lines_to_paragraphs_basic():
    elements: list[ContentDefinition] = [
        {
            "classification": "application/x-nldoc.element.text+line",
            "labels": [{"name": "has_neighbor_top", "confidence": 1.0}],
            "confidence": 0.9,
            "bbox": {"top": 10, "bottom": 20, "left": 10, "right": 100},
            "attributes": {"text": "Line 1"},
            "children": [],
        },
        {
            "classification": "application/x-nldoc.element.text+line",
            "labels": [{"name": "has_neighbor_top", "confidence": 1.0}],
            "confidence": 0.8,
            "bbox": {"top": 30, "bottom": 40, "left": 10, "right": 100},
            "attributes": {"text": "Line 2"},
            "children": [],
        },
    ]

    consecutive_lines_to_paragraphs(elements)

    # Check that original elements are marked for removal
    assert elements[0]["attributes"]["remove"] == "true"
    assert elements[1]["attributes"]["remove"] == "true"

    # Check new paragraph properties
    assert len(elements) == 3
    new_paragraph = elements[2]
    assert (
        new_paragraph["classification"] == "application/x-nldoc.element.text+paragraph"
    )
    assert new_paragraph["confidence"] == mean([0.9, 0.8])
    assert new_paragraph["attributes"]["text"] == "Line 1 Line 2"
    assert len(new_paragraph["children"]) == 2


def test_consecutive_lines_to_paragraphs_empty_sequence():
    elements = []
    consecutive_lines_to_paragraphs(elements)
    assert len(elements) == 0


def test_consecutive_lines_to_paragraphs_with_excluded_labels():
    elements: list[ContentDefinition] = [
        {
            "classification": "application/x-nldoc.element.text+line",
            "labels": [
                {"name": "has_neighbor_top", "confidence": 1.0},
                {"name": "table", "confidence": 0.9},
            ],
            "confidence": 0.9,
            "bbox": {"top": 10, "bottom": 11, "left": 10, "right": 100},
            "attributes": {"text": "Line 1"},
            "children": [],
        },
        {
            "classification": "application/x-nldoc.element.text+line",
            "labels": [{"name": "has_neighbor_top", "confidence": 1.0}],
            "confidence": 0.8,
            "bbox": {"top": 50, "bottom": 51, "left": 10, "right": 100},
            "attributes": {"text": "Line 2"},
            "children": [],
        },
    ]

    consecutive_lines_to_paragraphs(elements)
    assert len(elements) == 3


def test_lines_to_paragraphs_basic():
    elements: list[ContentDefinition] = [
        {
            "classification": "application/x-nldoc.element.text+line",
            "labels": [],
            "confidence": 0.9,
            "bbox": {"top": 10, "bottom": 20, "left": 10, "right": 100},
            "attributes": {"text": "Single line"},
            "children": [],
        }
    ]

    lines_to_paragraphs(elements)

    # Check original line is marked for removal
    assert elements[0]["attributes"]["remove"] == "true"

    # Check new paragraph properties
    assert len(elements) == 2
    new_paragraph = elements[1]
    assert (
        new_paragraph["classification"] == "application/x-nldoc.element.text+paragraph"
    )
    assert new_paragraph["confidence"] == 0.9
    assert new_paragraph["attributes"]["text"] == "Single line"
    assert len(new_paragraph["children"]) == 1


def test_lines_to_paragraphs_non_line_elements():
    elements: list[ContentDefinition] = [
        {
            "classification": "application/x-nldoc.element.text+paragraph",
            "confidence": 0.9,
            "labels": [],
            "bbox": {"top": 10, "bottom": 20, "left": 10, "right": 100},
            "attributes": {"text": "Already a paragraph"},
            "children": [],
        }
    ]

    original_length = len(elements)
    lines_to_paragraphs(elements)
    # Check that no new paragraph was created
    assert len(elements) == original_length


def test_lines_to_paragraphs_empty_attributes():
    elements: list[ContentDefinition] = [
        {
            "classification": "application/x-nldoc.element.text+line",
            "confidence": 0.9,
            "labels": [],
            "attributes": {},
            "bbox": {"top": 10, "bottom": 20, "left": 10, "right": 100},
            "children": [],
        }
    ]

    lines_to_paragraphs(elements)

    # Check that empty attributes are handled correctly
    assert elements[1]["attributes"]["text"] == ""
    assert "remove" in elements[0]["attributes"]


def test_lines_to_paragraphs_multiple_lines():
    elements: list[ContentDefinition] = [
        {
            "classification": "application/x-nldoc.element.text+line",
            "labels": [],
            "confidence": 0.9,
            "bbox": {"top": 10, "bottom": 20, "left": 10, "right": 100},
            "attributes": {"text": "Line 1"},
            "children": [],
        },
        {
            "classification": "application/x-nldoc.element.text+line",
            "labels": [],
            "confidence": 0.8,
            "bbox": {"top": 30, "bottom": 40, "left": 10, "right": 100},
            "attributes": {"text": "Line 2"},
            "children": [],
        },
    ]

    lines_to_paragraphs(elements)

    # Check that both lines were converted to paragraphs
    assert len(elements) == 4
    assert all(e["attributes"].get("remove") == "true" for e in elements[:2])
    assert all(
        e["classification"] == "application/x-nldoc.element.text+paragraph"
        for e in elements[2:]
    )
