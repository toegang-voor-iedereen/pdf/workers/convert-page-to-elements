from statistics import mean
from typing import List

from kimiworker import (
    ContentDefinition,
    get_enclosing_bbox,
)
from kimiworker.utils.content_definition import (
    find_consecutive_element_indices_with_label,
    has_label,
    merge_labels,
)


def handle_headings(elements: List[ContentDefinition]):
    """
    Groups and merges related heading elements in a document.

    Processes the list of elements to identify and combine heading-related content by:
    1. Finding consecutive elements labeled as section headers or titles
    2. Splitting groups at elements without top neighbors
    3. Merging related heading elements into single heading structures
    4. Marking original elements for removal and adding merged headings

    Args:
        elements (List[ContentDefinition]): List of document elements to process

    Side Effects:
        - Marks original heading elements for removal by setting attributes['remove']
        - Appends new merged heading elements to the input list
        - Original heading elements remain in list but marked for removal
    """
    heading_indices = []
    for label in ["section_header", "title"]:
        heading_indices.extend(
            find_consecutive_element_indices_with_label(elements, [label])
        )

    split_heading_indices = []

    for group in heading_indices:
        current_group = [group[0]]

        for idx in group[1:]:
            if has_label(elements[idx], "has_neighbor_top"):
                current_group.append(idx)
            else:
                split_heading_indices.append(current_group)
                current_group = [idx]

        if current_group:
            split_heading_indices.append(current_group)

    for indices in split_heading_indices:
        heading_elements = [elements[i] for i in indices]

        merged_heading: ContentDefinition = {
            "classification": "application/x-nldoc.element.text+heading",
            "labels": merge_labels(
                [], [e.get("labels", []) or [] for e in heading_elements]
            ),
            "confidence": mean(e.get("confidence", 0) for e in heading_elements),
            "bbox": get_enclosing_bbox(heading_elements),
            "attributes": {
                "text": " ".join(
                    filter(
                        None,
                        [
                            str(e.get("attributes", {}).get("text", ""))
                            for e in heading_elements
                        ],
                    )
                ),
                "level": heading_elements[0].get("attributes", {}).get("level", 1),
            },
            "children": heading_elements.copy(),
        }

        for element in heading_elements:
            element["attributes"]["remove"] = "true"
        elements.append(merged_heading)
