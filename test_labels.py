"""Tests for label helpers"""

from kimiworker import ContentDefinition
from shapely import STRtree
from shapely.geometry import Polygon

from labels import apply_region_labels

element: ContentDefinition = {
    "classification": "some-classification",
    "confidence": 100,
    "attributes": {},
    "bbox": {"top": 0.1, "left": 0.1, "right": 0.3, "bottom": 0.3},
    "labels": [{"name": "existing", "confidence": 95}],
    "children": [],
}

region1: ContentDefinition = {
    "classification": "some-classification",
    "confidence": 100,
    "attributes": {},
    "bbox": {"top": 0.0, "left": 0.0, "right": 0.2, "bottom": 0.2},
    "labels": [{"name": "region1", "confidence": 90}],
    "children": [],
}

region2: ContentDefinition = {
    "classification": "some-classification",
    "confidence": 100,
    "attributes": {},
    "bbox": {"top": 0.2, "left": 0.2, "right": 0.4, "bottom": 0.4},
    "labels": [{"name": "region2", "confidence": 85}],
    "children": [],
}

regions = [region1, region2]
polygons = [
    Polygon(
        [
            [region.get("bbox").get("left"), region.get("bbox").get("top")],
            [region.get("bbox").get("right"), region.get("bbox").get("top")],
            [region.get("bbox").get("right"), region.get("bbox").get("bottom")],
            [region.get("bbox").get("left"), region.get("bbox").get("bottom")],
            [region.get("bbox").get("left"), region.get("bbox").get("top")],
        ]
    )
    for region in regions
]
regions_tree = STRtree(polygons)


def test_apply_region_labels_intersection():
    apply_region_labels(element, regions, regions_tree)
    expected_labels = [
        {"name": "existing", "confidence": 95},
        {"name": "region1", "confidence": 90},
        {"name": "region2", "confidence": 85},
    ]
    assert (
        element["labels"] == expected_labels
    ), "Labels were not merged correctly for intersecting regions"


def test_apply_region_labels_no_intersection():
    element_no_intersect: ContentDefinition = {
        "classification": "some-classification",
        "confidence": 100,
        "attributes": {},
        "bbox": {"top": 0.5, "left": 0.5, "right": 0.7, "bottom": 0.7},
        "labels": [{"name": "isolated", "confidence": 99}],
        "children": [],
    }
    apply_region_labels(element_no_intersect, regions, regions_tree)
    expected_labels = [{"name": "isolated", "confidence": 99}]
    assert (
        element_no_intersect["labels"] == expected_labels
    ), "Labels changed incorrectly for non-intersecting regions"


def test_apply_region_labels_basic():
    # Create a simple element with bbox
    element: ContentDefinition = {
        "classification": "region-classification",
        "confidence": 100,
        "attributes": {},
        "bbox": {"left": 0, "top": 0, "right": 10, "bottom": 10},
        "labels": [{"name": "existing_label", "confidence": 100}],
        "children": [],
    }

    # Create a region that overlaps with the element
    region: ContentDefinition = {
        "classification": "region-classification",
        "confidence": 100,
        "attributes": {},
        "children": [],
        "bbox": {"left": 5, "top": 5, "right": 15, "bottom": 15},
        "labels": [{"name": "region_label", "confidence": 100}],
    }

    # Create region polygon for the STRtree
    region_polygon = Polygon(
        [
            [region["bbox"]["left"], region["bbox"]["top"]],
            [region["bbox"]["right"], region["bbox"]["top"]],
            [region["bbox"]["right"], region["bbox"]["bottom"]],
            [region["bbox"]["left"], region["bbox"]["bottom"]],
            [region["bbox"]["left"], region["bbox"]["top"]],
        ]
    )

    regions = [region]
    regions_tree = STRtree([region_polygon])

    # Apply the region labels
    apply_region_labels(element, regions, regions_tree)

    # Check if labels were merged correctly
    assert element.get("labels") is not None, "Labels array is None"
    assert any(
        label["name"] == "region_label" for label in element["labels"] or []
    ), "Region label not found"
    assert any(
        label["name"] == "existing_label" for label in element["labels"] or []
    ), "Parent label not found"


def test_apply_region_labels_no_overlap():
    # Create an element with bbox
    element: ContentDefinition = {
        "classification": "classification",
        "confidence": 100,
        "attributes": {},
        "bbox": {"left": 0, "top": 0, "right": 5, "bottom": 5},
        "labels": [{"name": "existing_label", "confidence": 100}],
        "children": [],
    }

    # Create a region that doesn't overlap
    region: ContentDefinition = {
        "classification": "region-classification",
        "confidence": 100,
        "attributes": {},
        "bbox": {"left": 20, "top": 20, "right": 25, "bottom": 25},
        "labels": [{"name": "region_label", "confidence": 100}],
        "children": [],
    }

    region_polygon = Polygon(
        [
            [region["bbox"]["left"], region["bbox"]["top"]],
            [region["bbox"]["right"], region["bbox"]["top"]],
            [region["bbox"]["right"], region["bbox"]["bottom"]],
            [region["bbox"]["left"], region["bbox"]["bottom"]],
            [region["bbox"]["left"], region["bbox"]["top"]],
        ]
    )

    regions = [region]
    regions_tree = STRtree([region_polygon])

    apply_region_labels(element, regions, regions_tree)

    # Check that only existing label remains
    assert element["labels"] == [{"name": "existing_label", "confidence": 100}]


def test_apply_region_labels_recursive():
    # Create an element with a child
    child: ContentDefinition = {
        "classification": "child-classification",
        "confidence": 100,
        "attributes": {},
        "bbox": {"left": 2, "top": 2, "right": 4, "bottom": 4},
        "labels": [{"name": "child_label", "confidence": 100}],
        "children": [],
    }

    parent: ContentDefinition = {
        "classification": "parent-classification",
        "confidence": 100,
        "attributes": {},
        "bbox": {"left": 0, "top": 0, "right": 10, "bottom": 10},
        "labels": [{"name": "parent_label", "confidence": 100}],
        "children": [child],
    }

    # Create a region that overlaps with both parent and child
    region: ContentDefinition = {
        "classification": "region-classification",
        "confidence": 100,
        "attributes": {},
        "bbox": {"left": 1, "top": 1, "right": 8, "bottom": 8},
        "labels": [{"name": "region_label", "confidence": 100}],
        "children": [],
    }

    region_polygon = Polygon(
        [
            [region["bbox"]["left"], region["bbox"]["top"]],
            [region["bbox"]["right"], region["bbox"]["top"]],
            [region["bbox"]["right"], region["bbox"]["bottom"]],
            [region["bbox"]["left"], region["bbox"]["bottom"]],
            [region["bbox"]["left"], region["bbox"]["top"]],
        ]
    )

    regions: list[ContentDefinition] = [region]
    regions_tree = STRtree([region_polygon])

    apply_region_labels(parent, regions, regions_tree)

    # Check that labels were applied to both parent and child
    assert parent.get("labels") is not None, "Labels array is None"
    assert any(label["name"] == "region_label" for label in parent["labels"] or [])
    assert any(label["name"] == "parent_label" for label in parent["labels"] or [])

    assert child.get("labels") is not None, "Labels array is None"
    assert any(label["name"] == "region_label" for label in child["labels"] or [])
    assert any(label["name"] == "child_label" for label in child["labels"] or [])


def test_apply_region_labels_empty_labels():
    # Test handling of None or empty labels
    element: ContentDefinition = {
        "classification": "classification",
        "confidence": 100,
        "attributes": {},
        "bbox": {"left": 0, "top": 0, "right": 10, "bottom": 10},
        "labels": None,
        "children": [],
    }

    region: ContentDefinition = {
        "classification": "region-classification",
        "confidence": 100,
        "attributes": {},
        "children": [],
        "bbox": {"left": 5, "top": 5, "right": 15, "bottom": 15},
        "labels": None,
    }

    region_polygon = Polygon(
        [
            [region["bbox"]["left"], region["bbox"]["top"]],
            [region["bbox"]["right"], region["bbox"]["top"]],
            [region["bbox"]["right"], region["bbox"]["bottom"]],
            [region["bbox"]["left"], region["bbox"]["bottom"]],
            [region["bbox"]["left"], region["bbox"]["top"]],
        ]
    )

    regions = [region]
    regions_tree = STRtree([region_polygon])

    apply_region_labels(element, regions, regions_tree)

    # Check that empty labels are handled correctly
    assert isinstance(element["labels"], list)
    assert len(element["labels"]) == 0
