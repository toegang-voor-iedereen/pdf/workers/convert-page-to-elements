from statistics import mean
from typing import List

from kimiworker import (
    ContentDefinition,
    get_enclosing_bbox,
)
from kimiworker.utils.content_definition import (
    find_consecutive_element_indices_with_label,
    merge_labels,
)


def should_include_previous_element(
    prev_element: ContentDefinition, curr_element: ContentDefinition
) -> bool:
    """
    Determines if the previous element should be included in the paragraph.

    Args:
        prev_element: The element before the consecutive sequence
        curr_element: The first element in the consecutive sequence

    Returns:
        bool: True if the previous element should be included in the paragraph
    """
    # Check if previous element has excluded labels
    excluded_labels = {
        "table",
        "picture",
        "page_footer",
        "section_header",
        "title",
        "page_number",
    }

    # Check if the current element has list_item label
    curr_labels = {label["name"] for label in (curr_element.get("labels") or [])}
    prev_labels = {label["name"] for label in (prev_element.get("labels") or [])}

    # Only exclude list_item if current element is not a list item
    if "list_item" not in curr_labels and "list_item" in prev_labels:
        return False

    if prev_labels & excluded_labels:
        return False

    # Get text content
    prev_text = str(prev_element.get("attributes", {}).get("text") or "")
    curr_text = str(curr_element.get("attributes", {}).get("text") or "")

    if not prev_text or not curr_text:
        return False

    # Check relative line lengths
    length_ratio = len(prev_text) / len(curr_text)
    if length_ratio < 0.4:
        return False

    return True


def consecutive_lines_to_paragraphs(elements: List[ContentDefinition]):
    """
    Combines consecutive text lines into paragraph structures.

    Processes the list of elements to find sequences of vertically adjacent text
    lines and merge them into paragraphs by:
    1. Finding consecutive elements with top neighbors that aren't tables, pictures,
        footers, list items, or headers
    2. Creating paragraph container elements for each sequence
    3. Merging labels, confidence scores, and text content
    4. Computing enclosing bounding boxes
    5. Marking original line elements for removal

    Args:
        elements (List[ContentDefinition]): List of document elements to process.
            Modified in-place to contain the new paragraph structures.

    Side Effects:
        - Marks original line elements for removal by setting attributes['remove']
        - Appends new paragraph elements to the input list
        - Original lines remain in list but marked for removal
    """
    paragraph_indices = find_consecutive_element_indices_with_label(
        elements,
        ["has_neighbor_top"],
        exclude_labels=[
            "table",
            "picture",
            "page_footer",
            "list_item",
            "section_header",
            "title",
        ],
    )

    for consecutive_indices in paragraph_indices:
        if not consecutive_indices:
            continue

        # Check if we should include the previous element
        start = consecutive_indices[0]
        if start > 0 and should_include_previous_element(
            elements[start - 1], elements[start]
        ):
            start -= 1

        end = consecutive_indices[-1] + 1
        paragraph_elements = elements[start:end]

        if not paragraph_elements:
            continue

        paragraph: ContentDefinition = {
            "classification": "application/x-nldoc.element.text+paragraph",
            "labels": merge_labels(
                [], [e.get("labels", []) or [] for e in paragraph_elements]
            ),
            "confidence": mean(e.get("confidence", 0) for e in paragraph_elements),
            "bbox": get_enclosing_bbox(paragraph_elements),
            "attributes": {
                "text": " ".join(
                    filter(
                        None,
                        [
                            str(e.get("attributes", {}).get("text", ""))
                            for e in paragraph_elements
                        ],
                    )
                )
            },
            "children": paragraph_elements.copy(),
        }

        for element in paragraph_elements:
            element["attributes"]["remove"] = "true"
        elements.append(paragraph)


def lines_to_paragraphs(elements: List[ContentDefinition]):
    """
    Converts individual text lines into single-line paragraph elements.

    Processes any remaining unconverted text lines by:
    1. Finding elements with text line classification
    2. Creating a paragraph container element for each line
    3. Copying over the line's labels, confidence, and text
    4. Using the original line's bounding box
    5. Marking the original line element for removal

    Args:
        elements (List[ContentDefinition]): List of document elements to process.
            Modified in-place to convert lines to paragraphs.

    Side Effects:
        - Marks original line elements for removal by setting attributes['remove']
        - Appends new paragraph elements to the input list
        - Original lines remain in list but marked for removal
    """
    lines = [
        e
        for e in elements
        if e["classification"] == "application/x-nldoc.element.text+line"
    ]
    for line in lines:
        paragraph: ContentDefinition = {
            "classification": "application/x-nldoc.element.text+paragraph",
            "labels": line.get("labels", []),
            "confidence": line.get("confidence", 0),
            "bbox": line.get("bbox", {"top": 0, "bottom": 0, "left": 0, "right": 0}),
            "attributes": {"text": str(line.get("attributes", {}).get("text", ""))},
            "children": [line.copy()],
        }
        if "attributes" not in line:
            line["attributes"] = {}
        line["attributes"]["remove"] = "true"
        elements.append(paragraph)
