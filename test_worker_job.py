import pytest
from worker_job import get_interpreted_content, get_regions
from kimiworker import WorkerJob


def test_get_interpreted_content_success():
    job: WorkerJob = {
        "recordId": "recordId",
        "bucketName": "bucket",
        "filename": "filename.pdf",
        "attributes": {
            "interpretedContent": {
                "bestJobId": "job1",
                "values": [{"jobId": "job1", "contentResult": [{"content": "test"}]}],
            }
        },
    }
    result = get_interpreted_content(job)
    assert result == [{"content": "test"}]


def test_get_interpreted_content_no_best_job():
    job: WorkerJob = {
        "recordId": "recordId",
        "bucketName": "bucket",
        "filename": "filename.pdf",
        "attributes": {"interpretedContent": {"bestJobId": None, "values": []}},
    }
    result = get_interpreted_content(job)
    assert result is None


def test_get_interpreted_content_no_matching_job():
    job: WorkerJob = {
        "recordId": "recordId",
        "bucketName": "bucket",
        "filename": "filename.pdf",
        "attributes": {
            "interpretedContent": {
                "bestJobId": "job1",
                "values": [{"jobId": "job2", "contentResult": [{"content": "test"}]}],
            }
        },
    }

    result = get_interpreted_content(job)
    assert result is None


def test_get_regions_success():
    job: WorkerJob = {
        "recordId": "recordId",
        "bucketName": "bucket",
        "filename": "filename.pdf",
        "attributes": {
            "regions": {
                "values": [
                    {"contentResult": [{"region": "1"}]},
                    {"contentResult": [{"region": "2"}]},
                ]
            }
        },
    }
    result = get_regions(job)
    assert result == [{"region": "1"}, {"region": "2"}]


def test_get_regions_empty():
    job: WorkerJob = {
        "recordId": "recordId",
        "bucketName": "bucket",
        "filename": "filename.pdf",
        "attributes": {"regions": {"values": None}},
    }
    result = get_regions(job)
    assert result == []
