from kimiworker import ContentDefinition
from kimiworker.utils.content_definition import has_label


def should_exclude_from_content(element: ContentDefinition) -> bool:
    """
    Determines if a content element should be excluded based on confidence and labels.

    Evaluates whether a content element should be excluded from processing by checking:
    1. If its confidence score is below 50%
    2. If it has any labels matching excluded types ('table' or 'picture')

    Args:
        element (ContentDefinition): Content element to evaluate.

    Returns:
        bool: True if the element should be excluded based on either:
            - Having a confidence score < 50 (defaults to 100 if not specified)
            - Having a label of 'table' or 'picture'
            False otherwise.
    """
    excluded_labels = ["table", "picture"]
    return element.get("confidence", 100) < 50 or any(
        has_label(element, label) for label in excluded_labels
    )
