from kimiworker import ContentDefinition

from content import should_exclude_from_content

# TODO: ContentDefinition confidence should be between 0 and 1, which is currently not the case


def test_excludes_low_confidence():
    """Should exclude elements with confidence below 50"""
    element: ContentDefinition = {"confidence": 49}  # type: ignore
    assert should_exclude_from_content(element) is True

    element = {"confidence": 0}  # type: ignore
    assert should_exclude_from_content(element) is True


def test_includes_high_confidence():
    """Should include elements with confidence 50 or above"""
    element: ContentDefinition = {"confidence": 50}  # type: ignore
    assert should_exclude_from_content(element) is False

    element = {"confidence": 100}  # type: ignore
    assert should_exclude_from_content(element) is False


def test_default_confidence():
    """Should default to confidence 100 if not specified"""
    element: ContentDefinition = {}  # type: ignore
    assert should_exclude_from_content(element) is False


def test_excludes_table():
    """Should exclude elements labeled as table regardless of confidence"""
    element: ContentDefinition = {"confidence": 100, "labels": [{"name": "table"}]}  # type: ignore
    assert should_exclude_from_content(element) is True


def test_excludes_picture():
    """Should exclude elements labeled as picture regardless of confidence"""
    element: ContentDefinition = {"confidence": 100, "labels": [{"name": "picture"}]}  # type: ignore
    assert should_exclude_from_content(element) is True


def test_includes_other_labels():
    """Should include elements with non-excluded labels"""
    element: ContentDefinition = {
        "confidence": 100,
        "labels": [{"name": "text"}, {"name": "paragraph"}],
    }  # type: ignore
    assert should_exclude_from_content(element) is False


def test_no_labels():
    """Should include elements with no labels if confidence is sufficient"""
    element: ContentDefinition = {"confidence": 100, "labels": []}  # type: ignore
    assert should_exclude_from_content(element) is False


def test_excludes_combined_conditions():
    """Should exclude elements that fail either condition"""
    # Low confidence with excluded label
    element: ContentDefinition = {"confidence": 40, "labels": [{"name": "table"}]}  # type: ignore
    assert should_exclude_from_content(element) is True

    # High confidence with excluded label
    element = {"confidence": 90, "labels": [{"name": "picture"}]}  # type: ignore
    assert should_exclude_from_content(element) is True

    # Low confidence with non-excluded label
    element = {"confidence": 40, "labels": [{"name": "text"}]}  # type: ignore
    assert should_exclude_from_content(element) is True
