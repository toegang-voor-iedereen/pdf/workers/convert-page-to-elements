from statistics import mean
from typing import List

from kimiworker import ContentDefinition

from heading import handle_headings


def test_single_section_header():
    """Should create a heading from a single section header element"""
    elements: List[ContentDefinition] = [
        {
            "labels": [{"name": "section_header", "confidence": 1.0}],
            "confidence": 90,
            "bbox": {"top": 0.1, "bottom": 0.2, "left": 0.1, "right": 0.9},
            "attributes": {"text": "Section 1", "level": 2},
        }
    ]  # type: ignore

    handle_headings(elements)

    assert len(elements) == 2  # Original + merged
    merged = elements[1]
    assert merged["classification"] == "application/x-nldoc.element.text+heading"
    assert merged["attributes"]["text"] == "Section 1"
    assert merged["attributes"]["level"] == 2
    assert elements[0]["attributes"]["remove"] == "true"


def test_single_title():
    """Should create a heading from a single title element"""
    elements: List[ContentDefinition] = [
        {
            "labels": [{"name": "title"}],
            "confidence": 90,
            "bbox": {"top": 0.1, "bottom": 0.2, "left": 0.1, "right": 0.9},
            "attributes": {"text": "Document Title"},
        }
    ]  # type: ignore

    handle_headings(elements)

    assert len(elements) == 2
    merged = elements[1]
    assert merged["classification"] == "application/x-nldoc.element.text+heading"
    assert merged["attributes"]["text"] == "Document Title"
    assert merged["attributes"]["level"] == 1  # Default level


def test_consecutive_headers_with_top_neighbor():
    """Should merge consecutive headers that are neighbors"""
    elements: List[ContentDefinition] = [
        {
            "labels": [{"name": "section_header", "confidence": 1.0}],
            "confidence": 90,
            "bbox": {"top": 0.1, "bottom": 0.2, "left": 0.1, "right": 0.9},
            "attributes": {"text": "Header", "level": 2},
        },
        {
            "labels": [
                {"name": "section_header", "confidence": 1.0},
                {"name": "has_neighbor_top"},
            ],
            "confidence": 85,
            "bbox": {"top": 0.2, "bottom": 0.3, "left": 0.1, "right": 0.9},
            "attributes": {"text": "Continued"},
        },
    ]  # type: ignore

    handle_headings(elements)

    assert len(elements) == 3  # Two originals + one merged
    merged = elements[2]
    assert merged["attributes"]["text"] == "Header Continued"
    assert merged["confidence"] == mean([90, 85])
    assert len(merged["children"] or []) == 2


def test_split_headers_without_top_neighbor():
    """Should create separate headings when headers aren't neighbors"""
    elements: List[ContentDefinition] = [
        {
            "labels": [{"name": "section_header", "confidence": 1.0}],
            "confidence": 90,
            "bbox": {"top": 0.1, "bottom": 0.2, "left": 0.1, "right": 0.9},
            "attributes": {"text": "Header 1"},
        },
        {
            "labels": [
                {"name": "section_header", "confidence": 1.0}
            ],  # No has_neighbor_top
            "confidence": 85,
            "bbox": {"top": 0.3, "bottom": 0.4, "left": 0.1, "right": 0.9},
            "attributes": {"text": "Header 2"},
        },
    ]  # type: ignore

    handle_headings(elements)

    assert len(elements) == 4  # Two originals + two merged
    assert elements[2]["attributes"]["text"] == "Header 1"
    assert elements[3]["attributes"]["text"] == "Header 2"


def test_mixed_content():
    """Should only process heading elements and ignore others"""
    elements: List[ContentDefinition] = [
        {
            "labels": [{"name": "section_header", "confidence": 1.0}],
            "confidence": 90,
            "bbox": {"top": 0.1, "bottom": 0.2, "left": 0.1, "right": 0.9},
            "attributes": {"text": "Header"},
        },
        {
            "labels": [{"name": "paragraph"}],
            "confidence": 85,
            "bbox": {"top": 0.2, "bottom": 0.3, "left": 0.1, "right": 0.9},
            "attributes": {"text": "Normal text"},
        },
    ]  # type: ignore

    handle_headings(elements)

    assert len(elements) == 3  # Original two + one merged heading
    assert elements[1]["attributes"].get("remove") is None  # Paragraph untouched


def test_empty_elements():
    """Should handle empty input without errors"""
    elements: List[ContentDefinition] = []
    handle_headings(elements)
    assert len(elements) == 0
