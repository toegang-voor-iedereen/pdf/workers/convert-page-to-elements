from itertools import chain
from typing import List

from kimiworker import (
    ContentDefinition,
    WorkerJob,
)
from pathdict import PathDict


def get_interpreted_content(job: WorkerJob) -> List[ContentDefinition] | None:
    """
    Retrieves the best interpreted content result from a worker job's attributes.

    This function extracts the interpreted content associated with the best job ID from
    the worker job's attributes. It first finds the ID of the best interpretation job,
    then locates and returns its corresponding content result.

    Args:
        job (WorkerJob): The worker job containing interpreted content attributes.

    Returns:
        List[ContentDefinition] | None: A list of ContentDefinition objects representing
            the interpreted content from the best job if found, None if:
            - No best job ID is found
            - No matching content is found for the best job ID
            - The job attributes lack the required data
    """
    attributes = PathDict(job.get("attributes"))

    best_job_id = attributes["interpretedContent.bestJobId"]
    if best_job_id is None:
        return

    values = attributes["interpretedContent.values"]
    best_value = next((x for x in values if x["jobId"] == best_job_id), None)
    if best_value is None:
        return

    return best_value["contentResult"]


def get_regions(job: WorkerJob) -> List[ContentDefinition]:
    """
    Extracts and flattens all region content definitions from a worker job.

    This function retrieves region data from the worker job's attributes, processes
    each region value to extract its content results, and combines them into a single
    flat list. If no regions are found, an empty list is returned.

    Args:
        job (WorkerJob): The worker job containing region data.

    Returns:
        List[ContentDefinition]: A flattened list of ContentDefinition objects from all
            region values. Returns an empty list if no regions are found or if the
            regions.values attribute is None.
    """
    attributes = PathDict(job.get("attributes"))
    values = attributes["regions.values"]

    if values is None:
        return []

    # get contentResult from each value
    values = [value["contentResult"] for value in values]

    # Flatten the array and return it
    return list(chain.from_iterable(values))
